# Master Channel Frames (MCF) transfer service example for base

import time

from src.uslp import UslpProtocolEntity, UslpFrame
from src.uslp.transport.udp import UdpTransport


from config import GROUND_ADDRESS, SATELLITE_ADDRESS, SCID, MCID


udp_transport = UdpTransport(
    routing={
        MCID: [SATELLITE_ADDRESS],
    }
)

udp_transport.bind(GROUND_ADDRESS)


def display_received_frame(frame):
    print("Received USLP Frame:", frame)
    attrs = vars(frame)
    print("  " + "\n  ".join("%s: %s" % item for item in attrs.items()))
    print()


entity = UslpProtocolEntity(transport=udp_transport)

entity.indication = display_received_frame

input("Running. Press <Enter> to stop...\n")

udp_transport.unbind()
