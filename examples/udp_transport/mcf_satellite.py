# Master Channel Frames (MCF) transfer service example

import time

from src.uslp import UslpProtocolEntity, UslpFrame
from src.uslp.transport.udp import UdpTransport


from config import GROUND_ADDRESS, SATELLITE_ADDRESS, SCID, MCID


udp_transport = UdpTransport(
    routing={
        MCID: [GROUND_ADDRESS],
    }
)

udp_transport.bind(SATELLITE_ADDRESS)


# def display_received_frame(frame):
#     print("Received USLP Frame:", frame)
#     attrs = vars(frame)
#     print("  " + "\n  ".join("%s: %s" % item for item in attrs.items()))
#     print()


entity = UslpProtocolEntity(transport=udp_transport)
# entity.indication = display_received_frame

try:
    input("Press <Enter> to start sending frames. Then press <Ctr-C> to stop.\n")
    i = 1

    while True:
        frame = UslpFrame(
            # transfer frame primary header
            tf_version=4,
            spacecraft_id=SCID,
            source_or_destination_id=0,
            vc_id=63,  # for idle frames (OID frames)
            map_id=0,
            end_of_frame_primary_header_flag=0,
            frame_length=None,  # to be calculated by the entity
            bypass_flag=0,
            control_command_flag=0,
            ocf_flag=0,
            vc_frame_count_length=1,
            vc_frame_count=i,
            # transfer frame insert zone
            insert_zone=None,
            # transfer frame data field
            tf_data_field_header=(0b001 << 21) | (0b11111 << 16) | 0xFFFF,  # TODO: see Figure 4-4
            tf_data_zone=0xFFFFFFFF,  # TODO: see Page 4-11 to continue here after
            # transfer frame operational control field
            tf_ocf=None,
            # transfer frame error control field
            tf_ecf=None,
        )
        print(bytearray(UslpFrame.encode(frame)))
        entity.request(frame, MCID)
        i += 1
        time.sleep(2)

except KeyboardInterrupt:
    pass

udp_transport.unbind()
