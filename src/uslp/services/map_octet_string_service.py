# variable length, delimited, octet-aligned data whose content and format are unknown to the SPP
# at least 1 and at most 65536 octets
# octet string may contain a packet secondary header defined by 4.1.4.2 of the SPP
# octet string shall be placed within the user data field of a single space packet
# the SPP will use the octet string service to transfer octet strings


# cannot exist when fixed-length transfer frames are used
# Based upon the QoS
# parameter selected by the user, either Sequence-Controlled or Expedited service can be
# provided

