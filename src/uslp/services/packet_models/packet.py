class PacketRequest:
    def __init__(self, space_packet, apid, quality_of_service = None):
        self.space_packet = space_packet
        self.apid = apid
        self.quality_of_service = quality_of_service

class PacketIndication:
    def __init__(self, space_packet, apid, packet_loss_indicator=None):
        self.space_packet = space_packet
        self.apid = apid
        self.packet_loss_indicator = packet_loss_indicator
