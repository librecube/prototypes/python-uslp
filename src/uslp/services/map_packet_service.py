# Also known as the MAPP Service
# if a COP is used, then both Sequence-Controlled and Expedited services are provided for the
# MAPP service. When the related managed parameter states that there is no COP in Effect
# only "best effort delivery" is provided for a map channel.
# when no reliable ARQ protocol is used, only expedited service is provided for a MAP channel
# that means that in this case a user identified with a single PVN and a GMAP ID.

from uslp.transport.services.packet_models import PacketRequest, PacketIndication
from uslp.constants import PVNVersion, MAXIMUM_PACKET_LENGTH_OCTETS


class MAPPacketService:
    def __init__(self, gmap_id, qos='expedited', max_packet_size=MAXIMUM_PACKET_LENGTH_OCTETS):
        """
        Initialize the MAPPacketService with the given GMAP ID, Quality of Service, and Maximum Packet Size.
        :param gmap_id: Global MAP ID associated with this service.
        :param qos: Quality of Service ('expedited' or 'sequence-controlled').
        :param max_packet_size: Maximum size of each packet (in bits).
        """
        self.gmap_id = gmap_id
        self.qos = qos
        self.max_packet_size = max_packet_size
        self.packet_queue = []
        self.notifications = []

    def request(self, sdu):
        """
        Request the processing and transmission of an SDU.
        :param sdu: The SDU to be processed and transmitted (in bits).
        """
        print(f"Request received for SDU of length {len(sdu)} bits")
        self.process_sdu(sdu)
        self.notify_indication("SDU processed and queued for transmission")

    def indication(self, sdu):
        """
        Indicate the arrival and processing of an SDU on the receiving side.
        :param sdu: The received SDU (in bits).
        """
        print(f"Indication: SDU of length {len(sdu)} bits received")
        # Process the received SDU as needed for the receiving application

    def notify_indication(self, message):
        """
        Notify the sender about the status of their request.
        :param message: The notification message.
        """
        print(f"Notify.Indication: {message}")
        self.notifications.append(message)

    def segment_sdu(self, sdu):
        """
        Segment an SDU into smaller packets and add them to the packet queue.
        :param sdu: The Service Data Unit to be segmented (in bits).
        """
        while len(sdu) > 0:
            packet = sdu[:self.max_packet_size]
            sdu = sdu[self.max_packet_size:]
            self.packet_queue.append(self.create_packet(packet))

    def create_packet(self, sdu):
        """
        Create a packet from an SDU.
        :param sdu: The Service Data Unit to be packed (in bits).
        :return: The formatted packet (in bits).
        """
        # Convert GMAP ID and QoS to bit representation
        gmap_id_bits = f'{self.gmap_id:016b}'  # Assuming 16 bits for GMAP ID
        qos_bits = '0' if self.qos == 'expedited' else '1'  # 0 for expedited, 1 for sequence-controlled
        length_bits = f'{len(sdu):016b}'  # Assuming 16 bits for length

        # Create packet header
        packet_header = gmap_id_bits + qos_bits + length_bits
        return packet_header + sdu

    def process_sdu(self, sdu):
        """
        Process an SDU by either segmenting or blocking it, and add it to the packet queue.
        :param sdu: The SDU to be processed (in bits).
        """
        if len(sdu) > self.max_packet_size:
            self.segment_sdu(sdu)
        else:
            self.packet_queue.append(self.create_packet(sdu))

    def send_packet(self):
        """
        Send the next packet in the queue.
        :return: The packet to be sent or None if queue is empty.
        """
        if self.packet_queue:
            packet = self.packet_queue.pop(0)
            # Placeholder: send packet to the transmission service
            print(f"Sending Packet: {packet}")
            self.notify_indication("Packet sent")
            return packet
        self.notify_indication("No more packets to send")
        return None

    def send_all_packets(self):
        """
        Send all packets in the queue, one by one.
        """
        while self.packet_queue:
            self.send_packet()

    def clear_queue(self):
        """
        Clear the packet queue.
        """
        self.packet_queue = []

    @staticmethod
    def extract_packets(tfdf):
        """
        Extract packets from a Transfer Frame Data Field (TFDF).
        :param tfdf: The Transfer Frame Data Field containing the packets (in bits).
        :return: A list of extracted packets (in bits).
        """
        extracted_packets = []
        while len(tfdf) > 0:
            # Assume a simple extraction based on known header format
            header_end = 33  # 16 bits for GMAP ID, 1 bit for QoS, 16 bits for length
            length_start = 17
            length_end = 33
            packet_length = int(tfdf[length_start:length_end], 2)

            packet_start = header_end
            packet_end = packet_start + packet_length
            packet = tfdf[packet_start:packet_end]
            extracted_packets.append(packet)
            tfdf = tfdf[packet_end:]

        return extracted_packets


def check_valid_version(pvn):
    if pvn not in PVNVersion:
        raise ValueError("Packet version number not authorized by CCSDS")


def packet_request(self, space_packet, apid, quality_of_service = None):
    request = PacketRequest(space_packet, apid, quality_of_service)
    return request


def packet_indication(self, space_packet, apid, quality_of_service = None):
    request = PacketIndication(space_packet, apid, quality_of_service)
    return request


# methods outside of python encoding, used for testing some bit implementations by hand
def translate_to_bits(message):
    binary_list = []
    # Padding the bit result with leading zeros to respect octets
    # This is on the basis of the naming convention 1.6.3 of CCSDS 133.0-B-2
    for char in message:
        binary_list.append(bin(ord(char))[2:].zfill(8))
    return ''.join(binary_list)


def bits_to_string(message):
    result_string = ""
    for i in range(0, len(message), 8):
        bin_char = message[i:i + 8]
        num = int(bin_char, 2)
        result_string += chr(num)
    return result_string
