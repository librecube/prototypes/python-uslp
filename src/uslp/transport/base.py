class BaseTransport:
    """An abstract transport class, to be implemented with relevant functionality."""

    def __init__(self):
        self.config = None

    def connect(self, *args, **kwargs):
        pass

    def disconnect(self, *args, **kwargs):
        pass

    def request(self, pdu, apid):
        raise NotImplementedError

    def indication(self, pdu):
        pass

    def shutdown(self):
        pass
