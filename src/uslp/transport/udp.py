import socket
import threading
import traceback

from .base import BaseTransport


DEFAULT_MAXIMUM_PACKET_LENGTH = 4096


class UdpTransport(BaseTransport):
    def __init__(
        self, routing, mapping=None, maximum_packet_length=DEFAULT_MAXIMUM_PACKET_LENGTH
    ):
        super().__init__()
        self.mapping = list(routing.keys())
        self.routing = routing
        self.maximum_packet_length = maximum_packet_length
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._thread = threading.Thread(target=self._incoming_pdu_handler)
        self._thread.kill = False
        self._address = None

    def bind(self, address):
        self._address = address
        self._socket.bind(address)
        self._thread.kill = False
        self._thread.start()

    def unbind(self):
        self._thread.kill = True
        self._socket.sendto(b"0", self._address)  # needed to break pdu handler loop
        self._thread.join()
        self._socket.close()

    def request(self, sdu, apid):
        if self.mapping:
            address = self.routing[apid]
            if address:
                for route in address:
                    host, port = route
                    port = int(port)
                    self._socket.sendto(sdu, (host, port))

    def _incoming_pdu_handler(self):
        thread = threading.current_thread()
        buffer_size = 10 * self.maximum_packet_length

        while not thread.kill:
            try:
                pdu, address = self._socket.recvfrom(buffer_size)
                if thread.kill:
                    break
                # forward-route the received pdu
                if self.routing and address in self.routing:
                    for dest in self.routing[address]:
                        self._socket.sendto(pdu, dest)
                self.indication(pdu)
            except Exception as e:
                print(
                    f"Exception in CustomUdpTransport._incoming_pdu_handler: {e} from {address}"
                )
                print(traceback.format_exc())
                continue  # Continue listening even after an exception
