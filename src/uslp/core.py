import math


class UslpProtocolEntity:
    def __init__(self, transport):
        self.transport = transport
        self.transport.indication = self._pdu_received

    def request(self, pdu, mc_id):
        self.transport.request(UslpFrame.encode(pdu), mc_id)

    def indication(self, frame, mc_id, frame_loss_flag=None):
        pass

    def _pdu_received(self, pdu):
        uslp_frame = UslpFrame.decode(pdu)
        self.indication(uslp_frame)


class UslpMasterChannelFrameProtocolEntity(UslpProtocolEntity):

    def request(self, frame, mc_id):
        self.transport.request(frame.encode(), mc_id)

    def indication(self, frame, mc_id, frame_loss_flag=None):
        # to be overwritten by higher layer user
        pass

    def _pdu_received(self, pdu):
        frame = UslpFrame.decode(pdu)
        if frame.vc_id == 63:
            mc_id = 0
        else:
            mc_id = frame.tf_version | frame.spacecraft_id
        self.indication(frame, mc_id)


class UslpFrame:
    def __init__(
            self,
            # transfer frame primary header
            tf_version,
            spacecraft_id,
            source_or_destination_id,
            vc_id,
            map_id,
            end_of_frame_primary_header_flag,
            frame_length,
            bypass_flag,
            control_command_flag,
            ocf_flag,
            vc_frame_count_length,
            vc_frame_count,
            # transfer frame insert zone
            insert_zone,
            # transfer frame data field
            tf_data_field_header,
            tf_data_zone,
            # transfer frame operational control field
            tf_ocf,
            # transfer frame error control field
            tf_ecf,
    ):
        # Transfer frame header checks
        # if not (0 <= tf_version < 16):  # 4 bits
        #     raise ValueError("Transfer Frame Version Number must be between 0 and 15")
        # if not (0 <= source_or_destination_id < 2):  # 1 bit
        #     raise ValueError("Source or Destination ID must be 0 or 1")
        # if not (0 <= vc_id < 64):  # 6 bits
        #     raise ValueError("Virtual Channel ID must be between 0 and 63")
        # if not (0 <= map_id < 16):  # 4 bits
        #     raise ValueError("MAP ID must be between 0 and 15")
        # if not (0 <= spacecraft_id < 65536):  # 16 bits
        #     raise ValueError("Spacecraft ID must be between 0 and 65535")
        # else:
        #     if frame_length is None:
        #         pass
        #     elif 0 <= frame_length < 32768:  # 15 bits for frame length because 1 bit is used by the flag
        #         raise ValueError("Frame Length must be between 0 and 32767")

        self.tf_version = tf_version
        self.spacecraft_id = spacecraft_id
        self.source_or_destination_id = source_or_destination_id
        self.vc_id = vc_id
        self.map_id = map_id
        self.end_of_frame_primary_header_flag = end_of_frame_primary_header_flag

        self.frame_length = frame_length
        self.bypass_flag = bypass_flag
        self.control_command_flag = control_command_flag
        self.ocf_flag = ocf_flag
        self.vc_frame_count_length = vc_frame_count_length
        self.vc_frame_count = vc_frame_count
        # transfer frame insert zone
        if insert_zone:
            self.insert_zone = insert_zone  # Optional for Insert Service
        else:
            self.insert_zone = None
        # transfer frame data field
        self.tf_data_field_header = tf_data_field_header
        self.tf_data_zone = tf_data_zone

        # transfer frame operational control field
        self.tf_ocf = tf_ocf

        # transfer frame error control field
        self.tf_ecf = tf_ecf

        if self.frame_length is None:
            # 7 bytes up until VC Frame Count always there
            self.frame_length = (
                    6
                    + math.ceil(vc_frame_count.bit_length()/8)
                    + math.ceil(tf_data_field_header.bit_length()/8)
                    + math.ceil(tf_data_zone.bit_length()/8)
            )
            if self.insert_zone:
                self.frame_length += math.ceil(self.insert_zone.bit_length()/8)
            if self.tf_ecf:
                self.frame_length += math.ceil(self.tf_ecf.bit_length()/8)
            if self.tf_ecf:
                self.frame_length += math.ceil(self.tf_ecf.bit_length()/8)

    def encode(self):
        header = bytearray()
        header.append(
            (self.tf_version << 4) | ((self.spacecraft_id >> 12) & 0xF)
        )
        header.append((self.spacecraft_id >> 4) & 0xFF)
        header.append(
            ((self.spacecraft_id & 0xF) << 4) | (self.source_or_destination_id << 3) | (self.vc_id >> 3)
        )
        header.append(
            ((self.vc_id & 0x7) << 5)
            | (self.map_id << 1)
            | self.end_of_frame_primary_header_flag
        )
        # End of common part of headers for truncated and non-truncated uslp frames

        # True for truncated frames
        if self.end_of_frame_primary_header_flag & 1 == 0b0:
            # Maybe the double check is warranted for value checking?
            header.append((self.frame_length >> 8) & 0xFF)
            header.append(self.frame_length & 0xFF)
            header.append(
                (self.bypass_flag << 7)
                | (self.control_command_flag << 6)
                | (self.ocf_flag << 3)
                | self.vc_frame_count_length
            )
            # END OF 6 BYTES OF THE PRIMARY HEADER
            # following bytes
            for i in range(self.vc_frame_count_length):
                shift_amount = 8 * (self.vc_frame_count_length - 1 - i)
                header.append((self.vc_frame_count >> shift_amount) & 0xFF)
        # TODO IN_SDU insert_zone not accounted for but it should be placed here
        if self.insert_zone:
            header.append(self.insert_zone)

        header.extend(bytearray(self.tf_data_field_header.to_bytes(math.ceil(self.tf_data_field_header.bit_length()/8), byteorder='big')))
        header.extend(self.tf_data_zone.to_bytes(math.ceil(self.tf_data_zone.bit_length()/8), byteorder='big'))

        if self.tf_ocf:
            header.append(self.tf_ocf)
        if self.tf_ecf:
            header.append(self.tf_ecf)

        return bytes(header)

    @classmethod
    def decode(cls, pdu):
        # Check if PDU has at least the minimum length for the primary header
        # if len(pdu) < 6:  # Minimum length considering mandatory fields
        #     raise ValueError("PDU is too short to contain a valid Transfer Frame")

        # Initialize the index pointer
        index = 0

        # Extract the Transfer Frame Primary Header
        tf_version = (pdu[index] >> 4) & 0x0F
        spacecraft_id = ((pdu[index] & 0x0F) << 12) | (pdu[index+1] << 4) | ((pdu[index+2] >> 4) & 0x0F)
        source_or_destination_id = (pdu[index+2] >> 3) & 0x01

        vc_id = ((pdu[index+2] & 0x07) << 3) | ((pdu[index+3] >> 5) & 0x07)
        if vc_id == 63:  # make sure the map id is set to 0 on idle frames
            map_id = 0
        else:
            map_id = (pdu[index+3] >> 1) & 0x0F

        end_of_frame_primary_header_flag = pdu[index+3] & 0x01
        frame_length = ((pdu[index+4] << 8) | pdu[index+5]) + 1

        # Validate that the total length matches the frame length
        if len(pdu) != frame_length:
            raise ValueError("PDU length does not match Frame Length")

        # Move index to the next field after the Primary Header
        index += 6  # 6 octets

        # Extract the flags with correct bit positions
        bypass_flag = (pdu[index] >> 7) & 0x01
        control_command_flag = (pdu[index] >> 6) & 0x01
        # Skip the spare bits (3rd and 4th bits) by just moving over them
        ocf_flag = (pdu[index] >> 3) & 0x01
        vc_frame_count_length = pdu[index] & 0x07  # 3 bits for VC Frame Count Length

        # Move index to the next field after the flags
        index += 1  # 7 octets

        # Handle VC Frame Count based on the VC Frame Count Length
        vc_frame_count = None  # Default to None if no VCF Count is present
        if vc_frame_count_length != 0:
            vc_frame_count_bytes = vc_frame_count_length  # Number of bytes = value of vc_frame_count_length
            vc_frame_count = int.from_bytes(pdu[index:index + vc_frame_count_bytes], byteorder='big')
            index += vc_frame_count_bytes
        # End of the primary header

        # Decode the Transfer Frame Insert Zone if present
        insert_zone = None
        if frame_length > 6 and pdu[index] & 0x80:
            # Logic to decode Insert Zone (length depends on implementation specifics)
            insert_zone = pdu[index]
            index += 1  # or more depending on Insert Zone length

        # Decode Transfer Frame Data Field Header and Data Zone
        tfdf_length = frame_length - index - 1  # -1 because the index is incremented after ops
        if ocf_flag == 1:
            tfdf_length -= 1
        # Assuming no error correction exists to decrement the length of tfdf
        if tfdf_length <= 0:
            raise ValueError("Transfer Frame Data Field Header invalid size")
        # TODO implement error correction check on the basis of the phys layer
        # TFDF can have up to 65529 octets (integer max)

        tfdz_construction_rule = (pdu[index] >> 5) & 0x7  # Simplified example
        uslp_protocol_id = pdu[index] & 0x1F
        index += 1

        tf_data_field_header = (tfdz_construction_rule << 5) | uslp_protocol_id

        first_header_pointer = None
        if tfdz_construction_rule in {0b000, 0b001, 0b010}:
            first_header_pointer = (pdu[index] << 8) | pdu[index + 1]  # 16 bits for the pointer
            index += 2

        if first_header_pointer:
            tf_data_field_header = (tf_data_field_header << 16) | first_header_pointer

        mc_id = tf_version | spacecraft_id
        # Decode the Transfer Frame Data Zone (TFDZ) based on the Construction Rule
        # TODO: convert the array of bits to make this work
        tf_data_zone, first_header_pointer = cls.__decode_tfdz(pdu, map_id, index, frame_length, ocf_flag, tfdz_construction_rule, mc_id)

        # Initialize an empty byte array
        byte_array = bytearray()

        # Process every 8 bits into a single byte
        for i in range(0, len(tf_data_zone), 8):
            byte = 0
            for bit_position in range(8):
                # Since each element is 0b1, we can directly shift it to the correct position and OR it into byte
                byte |= (tf_data_zone[i + bit_position] << (7 - bit_position))
            byte_array.append(byte)

        tf_data_zone = byte_array

        # Decode the Transfer Frame Operational Control Field (OCF) if present
        # TODO: implement OCF decoding
        tf_data_field_header = tf_data_field_header << 16 | first_header_pointer
        index += len(tf_data_zone) + 1

        # Decode the Operational Control Field (OCF) if present
        tf_ocf = None
        if ocf_flag:
            tf_ocf = pdu[index:index+4]
            index += 4

        # Decode the Frame Error Control Field (FECF)
        # Current implementation is considered for Presence of Frame Error Control = 0
        tf_ecf = None

        # Create and return the UslpFrame instance
        return cls(
            tf_version,
            spacecraft_id,
            source_or_destination_id,
            vc_id,
            map_id,
            end_of_frame_primary_header_flag,
            frame_length,
            bypass_flag,
            control_command_flag,
            ocf_flag,
            vc_frame_count_length,
            vc_frame_count,
            insert_zone,
            tf_data_field_header,
            tf_data_zone,
            tf_ocf,
            tf_ecf,
        )

    # Frame randomization for continuous data frames
    __oid_frame_randomizer = None

    @classmethod
    def __step_oid_frame_randomizer(cls):
        if cls.__oid_frame_randomizer is None:
            cls.__oid_frame_randomizer = [0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1,
                                          0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1,
                                          0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1,
                                          0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1]
        else:
            # Apply the feedback polynomial D^0 + D^1 + D^2 + D^22 + D^32
            feedback_bit = (cls.__oid_frame_randomizer[31]
                            ^ cls.__oid_frame_randomizer[21]
                            ^ cls.__oid_frame_randomizer[1]
                            ^ cls.__oid_frame_randomizer[0])

            # Shift the state and insert the feedback bit
            cls.__oid_frame_randomizer = [feedback_bit] + cls.__oid_frame_randomizer[:-1]

    @classmethod
    def __decode_tfdz(cls, pdu, map_id, index, frame_length, ocf_flag, tfdz_construction_rule, mc_id):
        index_increment = None
        if ocf_flag:
            index_increment = 4  # for returning remaining bytes up to the TF_OCF
        """Decode the Transfer Frame Data Zone based on the TFDZ Construction Rule."""
        if tfdz_construction_rule == 0b000:
            # Handle Packets Spanning Multiple Frames
            return pdu[index:frame_length-index_increment]
        elif tfdz_construction_rule == 0b001:
            # The TFDZ of an OID Transfer Frame shall be filled in by the mandatory Pseudo Noise(PN) sequence
            # generated by a 32 cell Linear Feedback Shift Register(LFSR) with polynomial D^0 + D^1 + D^2 + D^22 + D^32
            # these are initialized at deviced startup and a field should maintain the sequence
            # initially the all-one seed when the generator is used
            if map_id == 0:
                cls.__step_oid_frame_randomizer()
                return cls.__oid_frame_randomizer, 0xFFFF
            else:
                return pdu[index:frame_length-index_increment]
            # Handle Start of MAPA_SDU or VCA_SDU
        elif tfdz_construction_rule == 0b010:
            # Handle Continuing Portion of MAPA_SDU or VCA_SDU
            return pdu[index:frame_length-index_increment]
        elif tfdz_construction_rule == 0b011:
            # Handle Octet Stream
            return pdu[index:frame_length-index_increment]
        elif tfdz_construction_rule == 0b100:
            # Handle Starting Segment
            return pdu[index:frame_length-index_increment]
        elif tfdz_construction_rule == 0b101:
            # Handle Continuing Segment
            return pdu[index:frame_length-index_increment]
        elif tfdz_construction_rule == 0b110:
            # Handle Last Segment
            return pdu[index:frame_length-index_increment]
        elif tfdz_construction_rule == 0b111:
            # Handle No Segmentation
            return pdu[index:frame_length-index_increment]
        else:
            raise ValueError("Unknown TFDZ Construction Rule")
