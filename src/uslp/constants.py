# Constants for USLP Protocol
VCID_IDLE_PACKET = 0b111111  # Assuming VCID for idle packet as an example


MAXIMUM_PACKET_LENGTH_OCTETS = 128  # 128 bytes default, can be changed as it's a managed param
INCOMPLETE_PACKETS_TRANSFER = True  # managed param for packet transfer


class PVNVersion:  # managed param for packet transfer
    SPACE_PACKET = 0b000
    ENCAPSULATION_PACKET = 0b111
    UNASSIGNED = set(range(0b010, 0b110))


class USLPProtocolIdentifier:
    SPACE_OR_ENCAPSULATION_PACKET = 0b00000
    COP_1 = 0b00001
    COP_P2 = 0b00010
    SDLS = 0b00011
    OCTET_STREAM = 0b00100
    PROXIMITY_1 = 0b00111
    IDLE_DATA = 0b11111
    USED_NUMBERS = {
        SPACE_OR_ENCAPSULATION_PACKET,
        COP_1,
        COP_P2,
        SDLS,
        OCTET_STREAM,
        PROXIMITY_1,
        IDLE_DATA
    }
    # UNASSIGNED contains all 5-bit values not in used_bits
    UNASSIGNED = {i for i in range(32)} - USED_NUMBERS


class PacketType:
    IDLE = 0
    TELEMETRY = 1  # Reflects a general data packet for USLP
    TELECOMMAND = 2  # USLP specific telecommand


class SequenceFlags:
    CONTINUATION_SEGMENT = 0b00
    FIRST_SEGMENT = 0b01
    LAST_SEGMENT = 0b10
    UNSEGMENTED = 0b11


class ServiceType:
    MAP_PACKET_SERVICE = "MAP PACKET SERVICE"
    MAP_ACCESS_SERVICE = "MAP ACCESS SERVICE"
    MAP_OCTET_STRING_SERVICE = "MAP OCTET STRING SERVICE"
    COP_MANAGEMENT_SERVICE = "COP MANAGEMENT SERVICE"
    VC_PACKET_SERVICE = "VC PACKET SERVICE"
    VC_ACCESS_SERVICE = "VC ACCESS SERVICE"
    VC_FRAME_SERVICE = "VC FRAME SERVICE"
    USLP_MC_OCF_SERVICE = "USLP MASTER CHANNEL OCF SERVICE"
    MC_FRAME_SERVICE = "MASTER CHANNEL FRAME SERVICE"
    INSERT_SERVICE = "INSERT SERVICE"


class HeaderType:
    NON_TRUNCATED = 0
    TRUNCATED = 1


class SourceOrDestId:
    NON_TRUNCATED = 0
    TRUNCATED = 1
